-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Jul 30, 2019 at 02:41 AM
-- Server version: 5.7.25
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `db_photo`
--

-- --------------------------------------------------------

--
-- Table structure for table `cat_sexo`
--

CREATE TABLE `cat_sexo` (
  `id` int(11) UNSIGNED NOT NULL,
  `tipo_sexo` varchar(50) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cat_sexo`
--

INSERT INTO `cat_sexo` (`id`, `tipo_sexo`, `activo`, `created_at`, `updated_at`) VALUES
(1, 'Hombre', 1, '2019-05-18 16:53:36', '2019-05-18 17:22:57'),
(2, 'Mujer', 1, '2019-05-18 16:54:45', '2019-05-18 16:54:45');

-- --------------------------------------------------------

--
-- Table structure for table `citas`
--

CREATE TABLE `citas` (
  `id` int(11) NOT NULL,
  `num_cliente` int(11) NOT NULL,
  `num_paquete` int(11) NOT NULL,
  `num_ubicacion` int(11) NOT NULL,
  `fecha_cita` datetime NOT NULL,
  `estatus_cita` int(11) NOT NULL DEFAULT '1',
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `citas`
--

INSERT INTO `citas` (`id`, `num_cliente`, `num_paquete`, `num_ubicacion`, `fecha_cita`, `estatus_cita`, `activo`, `created_at`, `updated_at`) VALUES
(5, 2, 2, 4, '2019-06-13 10:50:00', 3, 1, '2019-06-14 01:28:57', '2019-06-20 22:41:28'),
(6, 1, 1, 6, '2019-06-13 10:25:00', 3, 1, '2019-06-14 01:31:39', '2019-06-18 22:51:44'),
(7, 1, 2, 4, '2019-06-17 03:05:00', 3, 1, '2019-06-15 20:05:36', '2019-06-18 02:01:46'),
(8, 3, 1, 3, '2019-06-18 04:00:00', 3, 1, '2019-06-18 01:42:52', '2019-06-18 22:51:28'),
(9, 3, 1, 3, '2019-06-18 16:06:00', 3, 1, '2019-06-18 01:42:52', '2019-06-18 17:49:29'),
(10, 2, 2, 3, '2019-06-18 16:06:49', 3, 1, '2019-06-18 22:43:09', '2019-06-18 22:48:24'),
(11, 2, 2, 2, '2019-06-20 21:06:40', 3, 1, '2019-06-19 06:19:52', '2019-06-22 01:26:57'),
(12, 3, 2, 7, '2019-06-22 14:06:01', 3, 1, '2019-06-20 22:38:23', '2019-06-21 00:08:47'),
(13, 2, 2, 6, '2019-06-22 12:06:28', 1, 1, '2019-06-20 22:38:50', '2019-06-20 22:38:50'),
(14, 1, 2, 5, '2019-06-22 18:06:52', 1, 1, '2019-06-20 22:43:41', '2019-06-20 22:43:41'),
(15, 4, 2, 6, '2019-06-23 16:06:06', 1, 1, '2019-06-21 22:16:25', '2019-06-21 22:16:25'),
(16, 4, 2, 6, '2019-06-26 22:06:12', 1, 1, '2019-06-24 22:01:44', '2019-06-24 22:01:44');

-- --------------------------------------------------------

--
-- Table structure for table `clientes`
--

CREATE TABLE `clientes` (
  `id` int(11) UNSIGNED NOT NULL,
  `nombre_cliente` varchar(100) DEFAULT NULL,
  `apellido_paterno` varchar(100) DEFAULT NULL,
  `apellido_materno` varchar(100) DEFAULT NULL,
  `sexo` int(11) DEFAULT NULL,
  `fecha_nacimiento` datetime DEFAULT NULL,
  `direccion` varchar(50) DEFAULT NULL,
  `cp` int(11) DEFAULT NULL,
  `correo_electronico` varchar(100) DEFAULT NULL,
  `telefono` varchar(10) DEFAULT NULL,
  `telefono_celular` varchar(10) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `clientes`
--

INSERT INTO `clientes` (`id`, `nombre_cliente`, `apellido_paterno`, `apellido_materno`, `sexo`, `fecha_nacimiento`, `direccion`, `cp`, `correo_electronico`, `telefono`, `telefono_celular`, `activo`, `created_at`, `updated_at`) VALUES
(1, 'hector hugo', 'vargas', 'acevedo', 1, '1989-06-21 00:00:00', 'calle alondra colonia primavera #307', 87134, 'hector.vargas@tam.gob.mx', NULL, '8342732302', 1, '2019-05-18 17:16:19', '2019-06-21 17:52:45'),
(2, 'juan de jesus', 'hernandez', 'gonzales', 1, '1999-01-12 00:00:00', 'calle cardenas #675 colonia tamaulipas', 87019, 'juan@gmail.com', NULL, '8345462391', 1, '2019-05-19 00:28:39', '2019-05-18 19:30:17'),
(3, 'Diana', 'Saldivar', 'Sifuentes', 2, '1989-06-21 00:00:00', 'calle fresno #754', 87023, 'diana@hotmail.com', NULL, '8342743493', 1, '2019-06-18 01:42:21', '2019-06-21 17:44:15'),
(4, 'jesus', 'gallardo', 'jimenez', 1, '1991-02-14 00:00:00', 'calle mazatlan #476, Col. Veteranos', 87098, 'hector_vargas89@hotmail.com', NULL, '8346273282', 1, '2019-06-21 22:15:15', '2019-06-21 22:15:15');

-- --------------------------------------------------------

--
-- Table structure for table `estatus_cita`
--

CREATE TABLE `estatus_cita` (
  `id` int(11) UNSIGNED NOT NULL,
  `nombre_estatus_cita` varchar(29) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `estatus_cita`
--

INSERT INTO `estatus_cita` (`id`, `nombre_estatus_cita`, `created_at`, `updated_at`) VALUES
(1, 'Abierta', '2019-06-13 20:00:04', '2019-06-13 20:00:04'),
(2, 'Suspendida', '2019-06-13 20:00:04', '2019-06-13 20:00:04'),
(3, 'Cerrada', '2019-06-13 20:00:12', '2019-06-13 20:00:12');

-- --------------------------------------------------------

--
-- Table structure for table `mensajes`
--

CREATE TABLE `mensajes` (
  `id` int(11) NOT NULL,
  `nombre_cliente` varchar(200) NOT NULL,
  `correo_electronico` varchar(100) NOT NULL,
  `aunto` varchar(200) NOT NULL,
  `mensaje` text NOT NULL,
  `activo` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mensajes`
--

INSERT INTO `mensajes` (`id`, `nombre_cliente`, `correo_electronico`, `aunto`, `mensaje`, `activo`, `created_at`, `updated_at`) VALUES
(1, 'hector vargas', 'hector@hotmail.com', 'hola', 'necesito una session de fotos pero yaaa', 0, '2019-06-15 14:01:52', '2019-06-15 19:48:41'),
(2, 'fsdfsdf', 'hector@hotmail.com', 'jkljjkj', 'jolajsuhanjksa', 0, '2019-06-15 14:10:45', '2019-06-15 19:28:50'),
(3, 'hugo vargas', 'hector@hotmail.com', 'hola buenos dias ', 'hola quiero expresar que estoy conforme con sus fotografias pero necesito que me tome otras fotos en el planetario ', 0, '2019-06-15 14:11:36', '2019-06-18 22:58:55'),
(4, 'diana saldivar', 'diana@hotmail.com', 'holaa', 'hola quiero una sesion de fotos lo mas rapido posibleeee', 0, '2019-06-15 14:29:57', '2019-06-18 01:09:58');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `paquetes`
--

CREATE TABLE `paquetes` (
  `id` int(11) UNSIGNED NOT NULL,
  `nombre_paquete` varchar(100) DEFAULT NULL,
  `precio_paquete` float DEFAULT NULL,
  `descripcion_paquete` text,
  `activo` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `paquetes`
--

INSERT INTO `paquetes` (`id`, `nombre_paquete`, `precio_paquete`, `descripcion_paquete`, `activo`, `created_at`, `updated_at`) VALUES
(1, 'PAQUETE GOLD', 800, 'Sesión de fotos en locación dentro de la ciudad de Victoria. (El precio no incluye la entrada a los lugares ni el costo por hacer la sesión de fotos en el lugar)\n60 minutos de sesión de fotos\n20 Fotografías digitales editadas, en alta resolución. Todas las fotografías se envían a tu correo electrónico.\n Si salen más, se entregan sin costo extra.', 1, '2019-05-19 21:48:55', '2019-05-19 21:49:39'),
(2, 'PAQUETE BÁSICO', 400, 'Sesión de fotos en locación dentro de la ciudad de Victoria. (El precio no incluye la entrada a los lugares ni el costo por hacer la sesión de fotos en el lugar)\n30 minutos de sesión de fotos\n10 Fotografías digitales editadas, en alta resolución. Todas las fotografías se envían a tu correo electrónico.\nSi salen más, se entregan sin costo extra.', 1, '2019-05-19 21:51:09', '2019-05-19 21:51:09');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ubicacion`
--

CREATE TABLE `ubicacion` (
  `id` int(11) UNSIGNED NOT NULL,
  `nombre_ubicacion` varchar(100) DEFAULT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `cp` int(11) DEFAULT NULL,
  `activo` int(11) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ubicacion`
--

INSERT INTO `ubicacion` (`id`, `nombre_ubicacion`, `direccion`, `cp`, `activo`, `created_at`, `updated_at`) VALUES
(1, 'Planetario', 'Blvd. Fidel Velazquez S/N, Horacio Terán, Cd Victoria, Tamps.', 87000, 1, '2019-05-19 00:57:48', '2019-05-19 00:57:48'),
(2, 'Paseo Mendez', 'Calle Francisco I. Madero 820, Zona Centro, 87000 Cd Victoria, Tamps.', 87000, 1, '2019-05-19 00:58:31', '2019-05-19 00:58:31'),
(3, 'Zoologico Tamatan', 'Zoológico Tamatán Calzada General Luis Caballero s/n Cd. Victoria, Tamaulipas, México', 87000, 1, '2019-05-19 00:59:45', '2019-06-18 21:01:48'),
(4, 'Parque Recreativo Tamatan', 'Sin Nombre de Col 21, Cd Victoria, Tamps.', 87070, 1, '2019-05-19 01:00:43', '2019-06-18 21:01:44'),
(5, 'Torre Bicentenario', 'Prolongación Blvd Praxedis Balboa, Área Pajaritos,  Cd Victoria, Tamps.', 87083, 1, '2019-05-19 01:01:32', '2019-05-19 01:01:32'),
(6, 'Estadio Marte R. Gomez Segura', 'Calle Alberto Carrera Torres 104, Pedro José Méndez, Cd Victoria, Tamps.', 87048, 1, '2019-05-19 01:04:19', '2019-05-19 01:04:19'),
(7, 'Avenida 17', 'CALLE 17, CENTRO, Cd Victoria, Tamps', 87000, 1, '2019-05-19 01:05:34', '2019-05-19 01:05:34');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_user` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_user` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tipo_user` int(11) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `name_user`, `first_user`, `email`, `password`, `remember_token`, `tipo_user`, `activo`, `created_at`, `updated_at`) VALUES
(1, 'hugo89', 'hector hugo', 'vargas acevedo', 'hector@hotmail.com', '$2y$10$mUnaN5LR5vD/QwySpzAsF.gv6htzWO1QQxeGKM/7EvUnaWay6DuSu', 'TKXTFG7InUtRwmzWIx9MXiYlu6JM89m1ZuOtPpOo7lmfjpVZgqQL6el9bv2F', 1, 1, '2019-05-18 20:11:40', '2019-06-21 21:31:32'),
(2, 'diana89', 'Diana', 'Saldivar Sifuentes', 'diana@gmail.com', '$2y$10$uaBHLCM3G0.DGr8Ws3QhQ.BXimwmyV4QmmTv3XYRUYugNrFTC.n.i', '2h6K0ocBDe5VGHVehelQAt7LRHDC3MAWDMWYZCo6AiX8XPVtQ1uhawWl50rF', 2, 1, '2019-06-21 21:20:24', '2019-06-21 21:20:24');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cat_sexo`
--
ALTER TABLE `cat_sexo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `citas`
--
ALTER TABLE `citas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `estatus_cita`
--
ALTER TABLE `estatus_cita`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mensajes`
--
ALTER TABLE `mensajes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paquetes`
--
ALTER TABLE `paquetes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `ubicacion`
--
ALTER TABLE `ubicacion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cat_sexo`
--
ALTER TABLE `cat_sexo`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `citas`
--
ALTER TABLE `citas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `estatus_cita`
--
ALTER TABLE `estatus_cita`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `mensajes`
--
ALTER TABLE `mensajes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `paquetes`
--
ALTER TABLE `paquetes`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ubicacion`
--
ALTER TABLE `ubicacion`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
