@extends('layouts.inicio')

@section('content')
<div class="col-md-12">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Usuarios</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="row">
        <div class="col-md-12">
         <a class="btn btn-primary btn-sm" href="{{ route('usuarios.create') }}"><i class="ti-plus menu-icon"></i> Nuevo Usuario</a>
         <div style="height: 5px;"></div>
         <div class="card">
                <div class="card-body">
                  
                  <div class="table-responsive">
                    <table class="table">
			          <thead>
			            <tr>
			             <th>Nickname Usuario</th>
			             <th>Nombre del Usuario</th>
			             <th>Apellido del Usuario</th>
			             <th>Email</th>
			             <th>Tipo Usuario</th>
			              <th >Acciones</th>
			            </tr>
			          </thead>
			          <tbody>
			            @foreach ($usuarios as $usuario)
			            <tr>
			                <td>{{ $usuario->name }}</td>
			                <td>{{ $usuario->name_user }}</td>
			                <td>{{ $usuario->first_user }}</td>
			                <td>{{ $usuario->email }}</td>
			                <td>
			                	<?php 
			                		if ($usuario->tipo_user == 1) {
			                			echo 'Administrador';
			                		}elseif($usuario->tipo_user == 2){
			                			echo 'Usuario';
			                		}

			                	 ?>
			                </td>
			               
			                	
			                
			                <td>
			                    <div class="dropdown">
								  <button class="btn btn-link dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								    <i class="fas fa-bars"></i>
								  </button>
								  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
								    <a class="dropdown-item" href="{{ route('usuarios.edit',$usuario->id)}}"><i class="ti-pencil  menu-icon"></i> Editar</a>
								    <form action="{{ route('usuarios.destroy',$usuario->id )}}" method="POST">

			                        @csrf
			                        @method('DELETE')
								    <button class="dropdown-item" type="submit"><i class="ti-trash  menu-icon"></i> Eliminar</button>
								    </form>
								  </div>
								</div>

			                </td>

			            </tr>
			            @endforeach
			          </tbody>
			        </table>
			        {!! $usuarios->links() !!}
                  </div>
                </div>
              </div>
        
        </div>
      </div>
      <!-- /.row -->
    </div>
  </div>
  <!-- /.box -->
</div>


@endsection