@extends('layouts.inicio')

@section('content')

<div class="col-12 grid-margin">
  <div class="card">
    <div class="card-body">
      <form class="form-sample">
        <p class="card-description">
          Informacion Personal
        </p>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Nickname</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" value="@isset($usuario){{ $usuario->name }}@endisset" name="name"  placeholder="Ingresa el Nickname" >
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Nombre(s)</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" value="@isset($usuario){{ $usuario->name_user }}@endisset" name="name_user" placeholder="Ingresa Nombre(s)">
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Apellido(s)</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" value="@isset($usuario){{ $usuario->first_user }}@endisset" name="first_user" placeholder="Ingresa Apellido(s)">
              </div>
            </div>
          </div>


          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Correo Electronico</label>
              <div class="col-sm-9">
                <input type="email" class="form-control" value="@isset($usuario){{ $usuario->email }}@endisset" name="email" placeholder="Ingresa Correo Electronico">
              </div>
            </div>
          </div>

          
        </div>
        <div class="row">
          <?php 
          if (@isset($usuario) == true) { ?>
          
          <?php }else{ ?>
            <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Password</label>
              <div class="col-sm-9">
                <input type="text" class="form-control"  name="password"  placeholder="Ingresa Password">
              </div>
            </div>
          </div>
          <?php  }
            ?>
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Tipo Usuario</label>
              <div class="col-sm-9">
                <select name="tipo_user" class="form-control">
                  @isset($usuario)<option  value="{{ $usuario->tipo_user }}">{{ $usuario->tipo_user }}</option>@endisset
                  <option value="0">Selecciona un Tipo usuario</option>
                  <option value="1">Administrador</option>
                  <option value="2">Usuario</option>
                </select>
              </div>
            </div>
          </div>
          
        </div>
        


        <div class="row">
          <div class="col-md-6">
          	<a href="{{ url('usuarios') }}" class="btn btn-inverse-secondary btn-fw"><i class="ti-back-left  menu-icon"></i> Regresar</a>
            <?php 

              if (@isset($usuario) == true) { ?>
               <button type="submit" class="btn btn-inverse-primary btn-fw btn-submit"><i class="ti-save     menu-icon"></i> Editar</button>
            <?php }else{ ?>
              <button type="submit" class="btn btn-inverse-primary btn-fw btn-submit"><i class="ti-save     menu-icon"></i> Guardar</button>
            <?php  }
            ?>
          </div>
        
        </div>
      </form>
    </div>
  </div>
</div>


<script type="text/javascript">
$('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      isRTL: false,
      lenguaje: 'es-es',
    })

    $(".btn-submit").click(function(e){

        e.preventDefault();

        var name = $("input[name=name]").val();

        var name_user = $("input[name=name_user]").val();

        var first_user = $("input[name=first_user]").val();

        var email = $("input[name=email]").val();

        var password = $("input[name=password]").val();

        var tipo_user = $("select[name=tipo_user]").val();

        $.ajax({

           type:"{{ ( isset($usuario) ? 'PUT' : 'POST' ) }}",

           url:"{{ ( isset($usuario) ) ? '/usuarios/' . $usuario->id : '/usuarios/create' }}",
           headers: {
			      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			     },
           data:{
           	name:name, 
           	name_user:name_user,
           	first_user:first_user, 
           	email:email,
           	password:password,
           	tipo_user:tipo_user, 
           	
           },
           
            success:function(data){
                swal({title: "Felicidades!", text: data.success, type: "success"}, function(){ location.href ="{{ url('usuarios') }}"; } );
            }


        });



  });

</script>
@endsection