@extends('layouts.inicio')

@section('content')


<div class="col-12 grid-margin">
  <div class="box-header with-border">
    <h3 class="box-title">Envio Emails</h3>
  </div>
  <div class="card">
    <div class="card-body">
       <form class="forms-sample" method="POST" action="{{ url('send/email') }}">
            @csrf

        @if(Session::has('success'))
          <div class="alert alert-success" role="alert">
            <h4>¡Felicidades!</h4>

            <p>Se ha enviado su correo electronico con exito.</p>
          </div>
        @endif
        <div class="row">
          
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Asunto</label>
              <div class="col-sm-9">
               <input type="text" class="form-control{{ $errors->has('subject') ? ' is-invalid' : '' }}" name="subject" placeholder="Escriba el Asunto" autofocus>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Cliente Mail</label>
              <div class="col-sm-9">
                 <select class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" autofocus>
                      <option >Selecciona</option>
                    @foreach ($pacientes as $paciente)
                      <option value="{{ $paciente->correo_electronico }}">{{ $paciente->nombre_cliente }} {{ $paciente->apellido_paterno }} {{ $paciente->apellido_materno }}</option>
                    @endforeach 
                  </select>
              </div>
            </div>
          </div>
        </div>
       
       <div class="row">
          <div class="col-md-12">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Mensaje</label>
              <div class="col-sm-9">
               <textarea  class="form-control {{ $errors->has('message') ? ' is-invalid' : '' }}" name="message" style="height: 180px;width: 100%;" placeholder="Escriba un Mensaje">
                </textarea>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
           <button type="submit" class="btn btn-primary mr-2"><i class="fas fa-paper-plane"></i> Enviar</button>
          </div>
        
        </div>
      </form>
    </div>
  </div>
</div>
  <script>
    $(function () {
      //Add text editor
      $("#compose-textarea").wysihtml5();
    });
  </script>

@endsection