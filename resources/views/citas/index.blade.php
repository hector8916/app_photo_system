@extends('layouts.inicio')

@section('content')
<div class="col-md-12">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Citas</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="row">
        <div class="col-md-12">
         <a class="btn btn-primary btn-sm" href="{{ route('citas.create') }}"><i class="ti-plus menu-icon"></i> Nueva Cita</a>
         <div style="height: 5px;"></div>
         <div class="card">
                <div class="card-body">
                  
                  <div class="table-responsive">
                    <table class="table">
			          <thead>
			            <tr>
			             <th>Nombre del cliente</th>
			             <th>Paquete</th>
			             <th>Ubicación</th>
			             <th>Fecha</th>
			             <th>Estatus</th>
			              <th >Acciones</th>
			            </tr>
			          </thead>
			          <tbody>
			            @foreach ($citas as $cita)
			            <tr>
			                <td>{{ $cita->_NombreCliente->nombre_cliente }} {{ $cita->_NombreCliente->apellido_paterno }} {{ $cita->_NombreCliente->apellido_materno }}</td>
			                <td>{{ $cita->_Paquete->nombre_paquete }}</td>
			                <td>{{ $cita->_Ubicacion->nombre_ubicacion }}</td>
			                <td>{{ $cita->fecha_cita }}</td>
			                <td>
			                	<?php 

			                		if ($cita->_Estatus->nombre_estatus_cita == "Abierta") {
			                			echo '<label class="badge badge badge-success">'.$cita->_Estatus->nombre_estatus_cita.'</label>';
			                		}elseif($cita->_Estatus->nombre_estatus_cita == "Suspendida"){
			                			echo  '<label class="badge badge-danger">'.$cita->_Estatus->nombre_estatus_cita.'</label>';
			                		}

			                	 ?>
			                	 
			               	</td>
			                
			                <td>
			                    <div class="dropdown">
								  <button class="btn btn-link dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								    <i class="fas fa-bars"></i>
								  </button>
								  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
								    <a class="dropdown-item" href="{{ route('citas.edit',$cita->id)}}"><i class="ti-pencil  menu-icon"></i> Editar</a>
								    <a class="dropdown-item" href="{{ route('citas.show',$cita->id)}}"><i class="fas fa-file-invoice"></i> Contrato</a>
								    <form action="{{ route('citas.destroy',$cita->id )}}" method="POST">

			                        @csrf
			                        @method('DELETE')
								    <button class="dropdown-item" type="submit"><i class="ti-trash  menu-icon"></i> Eliminar</button>
								    </form>
								  </div>
								</div>

			                </td>

			            </tr>
			            @endforeach
			          </tbody>
			        </table>
			        {!! $citas->links() !!}
                  </div>
                </div>
              </div>
        
        </div>
      </div>
      <!-- /.row -->
    </div>
  </div>
  <!-- /.box -->
</div>


@endsection