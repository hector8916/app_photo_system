@extends('layouts.inicio')

@section('content')

<div class="col-12 grid-margin">
  <div class="card">
    <div class="card-body">
      <form class="form-sample">
        <p class="card-description">
          Informacion De Cita
        </p>
        <?php 
          if (@isset($cita) == true) { ?>
            
        <?php }else{ ?>
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Agenda de Citas</h4>
                    
                    <div class="table-responsive" style="width: 100%;height: 280px;overflow: scroll;">
                      <table class="table table-hover">
                        <thead>
                          <tr>
                            <th>
                              Dia
                            </th>
                            <th>
                             Mes
                            </th>
                            <th>
                             Año
                            </th>
                            <th>
                             Hora
                            </th>
                            <th>
                             Estatus
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach ($fecha as $fechas)
                          <tr>
                            <?php  

                            list($fecha_actual,$hora) = explode(' ', $fechas->fecha_cita);
                            list($anio,$mes,$dia) = explode('-', $fecha_actual);

                            ?>
                            <td>{{ $dia }}</td>
                            <td>
                              <?php 

                              if ($mes == 1) {
                                echo 'Enero';

                              }elseif($mes == 2){
                                echo 'Febrero';

                              }elseif($mes == 3){
                                echo 'Marzo';

                              }elseif($mes == 4){
                                echo 'Abril';

                              }elseif($mes == 5){
                                echo 'Mayo';

                              }elseif($mes == 6){
                                echo 'Junio';

                              }elseif($mes == 7){
                                echo 'Julio';

                              }elseif($mes == 8){
                                echo 'Agosto';

                              }elseif($mes == 9){
                                echo 'Septiembre';

                              }elseif($mes == 10){
                                echo 'Octubre';

                              }elseif($mes == 11){
                                echo 'Noviembre';

                              }elseif($mes == 12){
                                echo 'Diciembre';

                              }
                              ?>
                            </td>
                            <td>{{ $anio }}</td>
                            <td>{{ $hora }}</td>
                            <td>
                            <?php 

                            if ($fechas->estatus_cita == 1) {
                              echo '<label class="badge badge badge-success">Abierta</label>';
                            }elseif($fechas->estatus_cita == 2){
                              echo  '<label class="badge badge-danger">Suspendida</label>';
                            }

                            ?>
                            </td>
                          </tr>
                          @endforeach 
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
            </div>
        <?php  }

         ?>
        
        <div style="height: 30px;"></div>
         <p class="card-description">
            Creación de Cita
          </p>
        <div class="row">
         
            <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Ubicacion</label>
              <div class="col-sm-9">
                <select class="form-control"  name="num_ubicacion" >
                @isset($cita)<option  value="{{ $cita->num_ubicacion }}">{{ $cita->_Ubicacion->nombre_ubicacion }}</option>@endisset
                  <option value="0">Selecciona Ubicacion</option>
                  @foreach ($ubicacion as $tipoubicacion)
                    <option value="{{ $tipoubicacion->id }}">{{ $tipoubicacion->nombre_ubicacion }}</option>
                  @endforeach 
                </select>
              </div>
            </div>
          </div>
          
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Paquete</label>
              <div class="col-sm-9">
                <select class="form-control"  name="num_paquete" >
                @isset($cita)<option  value="{{ $cita->num_paquete }}">{{ $cita->_Paquete->nombre_paquete }}</option>@endisset
                  <option value="0">Selecciona Paquete</option>
                  @foreach ($paquete as $tipopaquete)
                    <option value="{{ $tipopaquete->id }}">{{ $tipopaquete->nombre_paquete }},${{ $tipopaquete->precio_paquete }}</option>
                  @endforeach 
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          
          
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Nombre del Cliente</label>
              <div class="col-sm-9">
                <select class="form-control"  name="num_cliente" >
                @isset($cita)<option  value="{{ $cita->num_cliente }}">{{ $cita->_NombreCliente->nombre_cliente }} {{ $cita->_NombreCliente->apellido_paterno }} {{ $cita->_NombreCliente->apellido_materno }}</option>@endisset
                  <option value="0">Selecciona Nombre del Cliente</option>
                  @foreach ($cliente as $tipocliente)
                    <option value="{{ $tipocliente->id }}">{{ $tipocliente->nombre_cliente  }} {{ $tipocliente->apellido_paterno  }} {{ $tipocliente->apellido_materno  }}</option>
                  @endforeach 
                </select>
              </div>
            </div>
          </div>

         
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Fecha Cita</label>
              <div class="col-sm-9">
                <div class="form-group">
                  <input class="form-control " name="fecha_cita" type="text" id="datetimepicker" value="@isset($cita){{ $cita->fecha_cita }}@endisset" placeholder="Ingresa la Fecha y Hora">
                </div>
              </div>
            </div>
          </div>

        </div>
        
         <?php 

          if (@isset($cita) == true) { ?>

        <div class="row">
          
          
            <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Estatus de Cita</label>
              <div class="col-sm-9">
                <select class="form-control"  name="estatus_cita" >
                @isset($cita)<option  value="{{ $cita->estatus_cita }}">{{ $cita->_Estatus->nombre_estatus_cita }}</option>@endisset
                  <option value="0">Selecciona Estatus Cita</option>
                  @foreach ($estatus as $tipostatus)
                    <option value="{{ $tipostatus->id }}">{{ $tipostatus->nombre_estatus_cita  }} </option>
                  @endforeach 
                </select>
              </div>
            </div>
          </div>

        </div>
        <?php 
          }else{
            
          }
        ?>


        <div class="row">
          <div class="col-md-6">
          	<a href="{{ url('citas') }}" class="btn btn-inverse-secondary btn-fw"><i class="ti-back-left  menu-icon"></i> Regresar</a>
            <?php 

              if (@isset($cita) == true) { ?>
               <button type="submit" class="btn btn-inverse-primary btn-fw btn-submit"><i class="ti-save     menu-icon"></i> Editar</button>
            <?php }else{ ?>
              <button type="submit" class="btn btn-inverse-primary btn-fw btn-submit"><i class="ti-save     menu-icon"></i> Guardar</button>
            <?php  }
            ?>
          </div>
        
        </div>
      </form>
    </div>
  </div>
</div>


<script type="text/javascript">
$("#datetimepicker").datetimepicker({
  
    showMeridian: true,
    startView: 1,
    maxView: 1,
    format: 'yyyy-mm-dd hh:mm:ss',
    
    autoclose: true,
   
  });

    $(".btn-submit").click(function(e){

        e.preventDefault();

        var num_ubicacion = $("select[name=num_ubicacion]").val();

        var num_paquete = $("select[name=num_paquete]").val();

        var num_cliente = $("select[name=num_cliente]").val();

        var fecha_cita = $("input[name=fecha_cita]").val();

        var estatus_cita = $("select[name=estatus_cita]").val();



        $.ajax({

           type:"{{ ( isset($cita) ? 'PUT' : 'POST' ) }}",

           url:"{{ ( isset($cita) ) ? '/citas/' . $cita->id : '/citas/create' }}",
           headers: {
			      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			     },
           data:{
           	num_ubicacion:num_ubicacion, 
           	num_paquete:num_paquete,
           	num_cliente:num_cliente, 
            fecha_cita:fecha_cita,
           	estatus_cita:estatus_cita,
           	
           },
           
            success:function(data){
              swal({title: "Felicidades!", text: data.success, type: "success"}, function(){ location.href ="{{ url('citas') }}"; } );
            }


        });



  });

</script>
@endsection