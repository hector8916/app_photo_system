@extends('layouts.inicio')

@section('content')
<div class="card">
  <div class="card-body">
    
    <section class="invoice">
        <div id="areaImprimir" style="padding-right: 27px;">
          <!-- title row -->
          <div class="row">
            

            <div class="col-sm-4 invoice-col">
             
            </div>
            <div class="col-sm-4 invoice-col">
             <h2 class="page-header" style=" text-align: center;">
                <i class="fa fa-globe"></i> AdminLTE, Inc.
                <br>
                <p><strong>CONTRATO DE SERVICIO FOTOGRÁFICO</strong></p>
              </h2>
            </div>
            <div class="col-sm-4 invoice-col">
             
            </div>
            <!-- /.col -->
          </div>

          <div class="row">
            

            <div class="col-sm-5 invoice-col">
             
            </div>

            <div class="col-sm-7 invoice-col">
              <?php 
                $fecha = $citas['fecha_cita'];
                 
                list($anio,$mes,$dia) = explode("-",$fecha);
                list($dias) = explode(" ",$dia);


              ?>
             <strong>Acuerdo de Servicio fotográfico realizado en la fecha <?php echo $dias; ?> -
              <?php 

              if ($mes == 1) {
                echo 'Enero';

              }elseif($mes == 2){
                echo 'Febrero';

              }elseif($mes == 3){
                echo 'Marzo';

              }elseif($mes == 4){
                echo 'Abril';

              }elseif($mes == 5){
                echo 'Mayo';

              }elseif($mes == 6){
                echo 'Junio';

              }elseif($mes == 7){
                echo 'Julio';

              }elseif($mes == 8){
                echo 'Agosto';

              }elseif($mes == 9){
                echo 'Septiembre';

              }elseif($mes == 10){
                echo 'Octubre';

              }elseif($mes == 11){
                echo 'Noviembre';

              }elseif($mes == 12){
                echo 'Diciembre';

              }
              ?> - 
              <?php echo $anio; ?>    </strong><br>
            </div>
            <!-- /.col -->
          </div>
          
          <!-- info row -->
          <div class="row invoice-info">
            <div class="col-sm-8 invoice-col">
              
              <address>
                
                Entre el Fotográfo: Diana Saldivar Sifuentes<br>
                Dirección: Calle Alondra #307, Col.Primavera<br>
                
              </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-8 invoice-col">
            @foreach($cliente_id as $cliente)
              <address>
                Nombre del Cliente: {{ $cliente->nombre_cliente }} {{ $cliente->apellido_paterno }} {{ $cliente->apellido_materno }}<br>
                Dirección: {{ $cliente->direccion }}<br>
                Telefono: {{ $cliente->telefono }}<br>
                Telefono Móvil: {{ $cliente->telefono_celular }}<br>
                Correo Electronico: {{ $cliente->correo_electronico }}<br>
              </address>
            @endforeach
            </div>
            <!-- /.col -->
            <div class="col-sm-12 invoice-col">
              Para el presente contrato la Sra. Diana Saldivar Sifuentes, quien adelante será referida como <strong>Fotográfa</strong>
              y el/la Sr(a) {{ $cliente->nombre_cliente }} {{ $cliente->apellido_paterno }} {{ $cliente->apellido_materno }} . quien adelante será referido(a) como el <strong>Cliente</strong> ha acordado suscribir el siguiente contrato de 
              servicios profesionales cuyas pautas son las siguientes:<br>
              
            </div>
            <div style="height: 80px;"></div>
            @foreach($ubicacion_id as $ubicacion)
              
            @endforeach
            <div class="col-sm-12 invoice-col" style="left: 30px;">
             1. El <strong>Fotográfo</strong> realizara la cobertura fotográfica para el <strong>Cliente</strong> en la fecha siguiente acordada:<br>
             <?php 
                $fecha = $citas['fecha_cita'];
                 
                list($anio,$mes,$dia) = explode("-",$fecha);
                list($dias,$horario) = explode(" ",$dia);

           

              ?>
              <?php echo $dias; ?> (día) 
              <?php 

                if ($mes == 1) {
                  echo 'Enero';
                  
                }elseif($mes == 2){
                  echo 'Febrero';

                }elseif($mes == 3){
                  echo 'Marzo';

                }elseif($mes == 4){
                  echo 'Abril';

                }elseif($mes == 5){
                  echo 'Mayo';

                }elseif($mes == 6){
                  echo 'Junio';

                }elseif($mes == 7){
                  echo 'Julio';

                }elseif($mes == 8){
                  echo 'Agosto';

                }elseif($mes == 9){
                  echo 'Septiembre';

                }elseif($mes == 10){
                  echo 'Octubre';

                }elseif($mes == 11){
                  echo 'Noviembre';

                }elseif($mes == 12){
                  echo 'Diciembre';

                }
                ?> (mes) <?php echo $anio; ?> (año)- en horario de <?php echo $horario; ?>, cuya celebraciónserá realizada en:  {{ $ubicacion->nombre_ubicacion }}.<br>  
            </div>
            <div style="height: 60px;"></div>
            @foreach($paquete as $monto)
              
            @endforeach
            <div class="col-sm-12 invoice-col" style="left: 30px;">
             2. El <strong>Cliente</strong> realizara el pago de  <strong>$ {{ $monto->precio_paquete }}.00</strong> pesos, de la siguiente manera<br>
              
            </div>
            <div style="height: 60px;"></div>
            <div class="col-sm-12 invoice-col" style="left: 60px;">
             a. El <strong>Pago</strong> de  <strong>$ {{ $monto->precio_paquete }}.00</strong> pesos, a la firma de este contrato equivale al 50 por ciento del valor total del servicio prestado.<br>
             b. El Balance restante de  <strong>$ {{ $monto->precio_paquete }}.00</strong> pesos, al realizarse la entrega final el material fotográfico (fisico).<br>
              
            </div>
            <div style="height: 60px;"></div>
            <div class="col-sm-12 invoice-col" style="left: 75px;">
             b1. La Entrega del material fotográfico es físico se realizara en un periodo de 10 dias hábiles contados a partir del día hábil <br>siguiente a la finalización del evento.<br>
             El material fotográfico electrónico se realizara a los 3 dias hábiles transcurridos después e la fecha del evento.
              
            </div>
            <div style="height: 100px;"></div>
            <div class="col-sm-12 invoice-col" style="left: 30px;">
              3. El <strong>Fotográfo</strong> será el profesional exclusivo  ejecutar las tomas fotográficas. (A otras personas
              les será permitido tomar  fotográfias <br> siempre  que a discreción del  <strong>Fotográfo</strong> los mismos no 
              obstruyan su labor).
            </div>
            <div style="height: 60px;"></div>
            <div class="col-sm-12 invoice-col" style="left: 30px;">
              4. Se acuerda expresamente en este contrato todos los derechos de uso y derechos de Autor.
              incluidos los derechos sobre negativos <br> y archivos digitales , de imagenes y cualquier otro material producido  
              por el <strong>Fotográfo</strong> y que cualquier reporducción del material <br> se reserva al <strong>Fotográfo</strong> y su beneficio comercial.
            </div>

            <div style="height: 100px;"></div>

            <div class="col-sm-12 invoice-col" style="left: 30px;">
              5. En el supuesto evento de fuerza mayor por el cual el <strong>Fotográfo</strong> se vea en la imposibilidad de realizar la cobertura del evento,<br> el <strong>Fotográfo</strong> 
              realizara el mejor esfuerzo para asegurar la cobertura del evento por medio el uso  de los servicos de otro profesional <br>  calificado. Sin embargo 
              la responsabilidad legal del <strong>Fotográfo</strong> bajo cualquier circunstancia se limitara a la devolución total del depósito <br> realizado por el <strong>Cliente</strong>, según conste en el contrato firmado y el pagare firmado y sellada por el <strong>Fotográfo</strong>, debidamente entregada al <strong>Cliente</strong>.No obstante la no presentación por parte del <strong>Cliente</strong> no implica la no devolución del monto acordado en el contrato.
            </div>

            <div style="height: 130px;"></div>

            <div class="col-sm-12 invoice-col" style="left: 30px;">
              6. El <strong>Cliente</strong> podrá realizar la cancelación del presente contrato,por cualquiera sea el motivo.<br>
              Sin embargo, el <strong>Cliente</strong> entiende y acepta que el monto pagado como depósito no le será devuelto. Dicho monto cubre los gastos <br> incurridos por el <strong>Fotográfo</strong>.
            </div>

            <div style="height: 150px;"></div>
            <div class="col-sm-6 invoice-col" style="left: 40px;">
              _________________________________________
            </div>

            <div class="col-sm-6 invoice-col" style="left: 40px;">
              _________________________________________
            </div>

            <div class="col-sm-6 invoice-col" style="left: 50px;">
              Firma <strong>{{ $cliente->nombre_cliente }} {{ $cliente->apellido_paterno }} {{ $cliente->apellido_materno }}</strong>
            </div>

            <div class="col-sm-6 invoice-col" style="left: 50px;">
              Firma <strong>Diana Saldivar Sifuentes</strong>
            </div>
            <!-- /.col -->
            <div style="height: 150px;"></div>
          </div>

          <!-- /.row -->
        </div>
          <!-- this row will not appear when printing -->
          <div class="row no-print">
            <div class="col-xs-12">
              <a onclick="printDiv('areaImprimir')" target="_blank" class="btn btn-primary" style="color:white;cursor: pointer"><i class="fa fa-print"></i> Imprimir</a>
              
            </div>
          </div>
    </section>
  </div>
</div>
<script type="text/javascript">
  function printDiv(nombreDiv) {
     var contenido= document.getElementById(nombreDiv).innerHTML;
     var contenidoOriginal= document.body.innerHTML;

     document.body.innerHTML = contenido;

     window.print();

     document.body.innerHTML = contenidoOriginal;
}
</script>
@endsection