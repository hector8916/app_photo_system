@extends('layouts.inicio')

@section('content')

<div class="col-12 grid-margin">
  <div class="card">
    <div class="card-body">
      <form class="form-sample">
        <p class="card-description">
          Informacion de Paquete
        </p>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Nombre del Paquete</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" value="@isset($paquete){{ $paquete->nombre_paquete }}@endisset" name="nombre_paquete"  placeholder="Ingresa el nombre de la paquete)" >
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Precio</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" value="@isset($paquete){{ $paquete->precio_paquete }}@endisset" name="precio_paquete" placeholder="Ingresa la precio">
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Descripción</label>
              <div class="col-sm-9">
                <textarea name="descripcion_paquete" class="form-control" rows="5">@isset($paquete){{ $paquete->descripcion_paquete }}@endisset</textarea>
                
              </div>
            </div>
          </div>

        </div>
        <div class="row">
          <div class="col-md-6">
          	<a href="{{ url('paquetes') }}" class="btn btn-inverse-secondary btn-fw"><i class="ti-back-left  menu-icon"></i> Regresar</a>
            <?php 

              if (@isset($paquete) == true) { ?>
               <button type="submit" class="btn btn-inverse-primary btn-fw btn-submit"><i class="ti-save  menu-icon"></i> Editar</button>
            <?php }else{ ?>
              <button type="submit" class="btn btn-inverse-primary btn-fw btn-submit"><i class="ti-save  menu-icon"></i> Guardar</button>
            <?php  }
            ?>
          </div>
        
        </div>
      </form>
    </div>
  </div>
</div>


<script type="text/javascript">
    $(".btn-submit").click(function(e){

        e.preventDefault();

        var nombre_paquete = $("input[name=nombre_paquete]").val();

        var precio_paquete = $("input[name=precio_paquete]").val();

        var descripcion_paquete = $("textarea[name=descripcion_paquete]").val();



        $.ajax({

           type:"{{ ( isset($paquete) ? 'PUT' : 'POST' ) }}",

           url:"{{ ( isset($paquete) ) ? '/paquetes/' . $paquete->id : '/paquetes/create' }}",
           headers: {
			      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			     },
           data:{
           	nombre_paquete:nombre_paquete, 
           	precio_paquete:precio_paquete,
           	descripcion_paquete:descripcion_paquete, 
           },
           
            success:function(data){
                swal({title: "Felicidades!", text: data.success, type: "success"}, function(){ location.href ="{{ url('paquetes') }}"; } );
            }


        });



  });

</script>
@endsection