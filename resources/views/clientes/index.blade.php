@extends('layouts.inicio')

@section('content')
<div class="col-md-12">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Clientes</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="row">
        <div class="col-md-12">
         <a class="btn btn-primary btn-sm" href="{{ route('clientes.create') }}"><i class="ti-plus menu-icon"></i> Nuevo Cliente</a>
         <div style="height: 5px;"></div>
         <div class="card">
                <div class="card-body">
                  
                  <div class="table-responsive">
                    <table class="table">
			          <thead>
			            <tr>
			             <th>Nombre del cliente</th>
			             <th>Apellido Paterno</th>
			             <th>Apellido Materno</th>
			             <th>Telefono Celular</th>
			             <th>Direccion</th>
			             <th>Sexo</th>
			              <th >Acciones</th>
			            </tr>
			          </thead>
			          <tbody>
			            @foreach ($clientes as $cliente)
			            <tr>
			                <td>{{ $cliente->nombre_cliente }}</td>
			                <td>{{ $cliente->apellido_paterno }}</td>
			                <td>{{ $cliente->apellido_materno }}</td>
			                <td>{{ $cliente->telefono_celular }}</td>
			                <td>{{ $cliente->direccion }}</td>
			                <td>{{ $cliente->_Tiposexo->tipo_sexo }}</td>
			                <td>

			                	<div class="dropdown">
		                            <button class="btn btn-link dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		                              <i class="fas fa-bars"></i>
		                            </button>
		                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
		                              <a class="dropdown-item" href="{{ route('clientes.edit',$cliente->id)}}"><i class="ti-pencil  menu-icon"></i> Editar</a>
		                              
		                              <form action="{{ route('clientes.destroy',$cliente->id )}}" method="POST">

		                                        @csrf
		                                        @method('DELETE')
		                              <button class="dropdown-item" type="submit"><i class="ti-trash  menu-icon"></i> Eliminar</button>
		                              </form>
		                            </div>
		                          </div>


			                </td>
			            </tr>
			            @endforeach
			          </tbody>
			        </table>
			        {!! $clientes->links() !!}
                  </div>
                </div>
              </div>
        
        </div>
      </div>
      <!-- /.row -->
    </div>
  </div>
  <!-- /.box -->
</div>


@endsection