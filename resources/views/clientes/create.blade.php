@extends('layouts.inicio')

@section('content')

<div class="col-12 grid-margin">
  <div class="card">
    <div class="card-body">
      <form class="form-sample">
        <p class="card-description">
          Informacion Personal
        </p>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Nombre(s)</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" value="@isset($cliente){{ $cliente->nombre_cliente }}@endisset" name="nombre_cliente"  placeholder="Ingresa el Nombre(s)" >
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Apellido Paterno</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" value="@isset($cliente){{ $cliente->apellido_paterno }}@endisset" name="apellido_paterno" placeholder="Ingresa Apellido Paterno">
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Apellido Materno</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" value="@isset($cliente){{ $cliente->apellido_materno }}@endisset" name="apellido_materno" placeholder="Ingresa Apellido Materno">
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Sexo</label>
              <div class="col-sm-9">
                <select class="form-control"  name="sexo" >
                @isset($cliente)<option  value="{{ $cliente->sexo }}">{{ $cliente->_Tiposexo->tipo_sexo}}</option>@endisset
                  <option value="0">Selecciona Sexo</option>
                  @foreach ($sexo as $tiposexo)
                    <option value="{{ $tiposexo->id }}">{{ $tiposexo->tipo_sexo }}</option>
                  @endforeach 
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Fecha de Nacimiento</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" value="@isset($cliente){{ $cliente->fecha_nacimiento }}@endisset" name="fecha_nacimiento" id="datepicker" placeholder="Ingresa Fecha de Nacimiento">
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Correo Electronico</label>
              <div class="col-sm-9">
                <input type="email" class="form-control" value="@isset($cliente){{ $cliente->correo_electronico }}@endisset" name="correo_electronico" placeholder="Ingresa Correo Electronico">
              </div>
            </div>
          </div>
          
        </div>
        <p class="card-description">
          Dirección
        </p>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Direccion</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" value="@isset($cliente){{ $cliente->direccion }}@endisset" name="direccion" placeholder="Ingresa Dirección">
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Codigo Postal</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" value="@isset($cliente){{ $cliente->cp }}@endisset" name="cp" placeholder="Ingresa Codigo Postal">
              </div>
            </div>
          </div>
          
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Telefono Casa</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" value="@isset($cliente){{ $cliente->telefono }}@endisset" name="telefono" placeholder="Ingresa Telefono de Casa">
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Telefono Celular</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" value="@isset($cliente){{ $cliente->telefono_celular }}@endisset" name="telefono_celular" placeholder="Ingresa Telefono Celular">
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
          	<a href="{{ url('clientes') }}" class="btn btn-inverse-secondary btn-fw"><i class="ti-back-left  menu-icon"></i> Regresar</a>
            <?php 

              if (@isset($cliente) == true) { ?>
               <button type="submit" class="btn btn-inverse-primary btn-fw btn-submit"><i class="ti-save     menu-icon"></i> Editar</button>
            <?php }else{ ?>
              <button type="submit" class="btn btn-inverse-primary btn-fw btn-submit"><i class="ti-save     menu-icon"></i> Guardar</button>
            <?php  }
            ?>
          </div>
        
        </div>
      </form>
    </div>
  </div>
</div>


<script type="text/javascript">
$('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      isRTL: false,
      lenguaje: 'es-es',
    })

    $(".btn-submit").click(function(e){

        e.preventDefault();

        var nombre_cliente = $("input[name=nombre_cliente]").val();

        var apellido_paterno = $("input[name=apellido_paterno]").val();

        var apellido_materno = $("input[name=apellido_materno]").val();

        var direccion = $("input[name=direccion]").val();

        var cp = $("input[name=cp]").val();

        var fecha_nacimiento = $("input[name=fecha_nacimiento]").val();

        var sexo = $("select[name=sexo]").val();

        var correo_electronico = $("input[name=correo_electronico]").val();

        var telefono = $("input[name=telefono]").val();

        var telefono_celular = $("input[name=telefono_celular]").val();


        $.ajax({

           type:"{{ ( isset($cliente) ? 'PUT' : 'POST' ) }}",

           url:"{{ ( isset($cliente) ) ? '/clientes/' . $cliente->id : '/clientes/create' }}",
           headers: {
			      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			     },
           data:{
           	nombre_cliente:nombre_cliente, 
           	apellido_paterno:apellido_paterno,
           	apellido_materno:apellido_materno, 
           	direccion:direccion,
           	cp:cp,
           	fecha_nacimiento:fecha_nacimiento, 
           	sexo:sexo,
           	correo_electronico:correo_electronico, 
           	telefono:telefono,
           	telefono_celular:telefono_celular
           },
           
            success:function(data){
                swal({title: "Felicidades!", text: data.success, type: "success"}, function(){ location.href ="{{ url('clientes') }}"; } );
            }


        });



  });

</script>
@endsection