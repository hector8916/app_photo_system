<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>VarySal</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="{{ asset('royal/vendors/ti-icons/css/themify-icons.css') }}">
  <link rel="stylesheet" href="{{ asset('royal/vendors/base/vendor.bundle.base.css') }}">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="{{ asset('royal/css/style.css') }}">
  <!-- endinject -->
  <link rel="shortcut icon" href="{{ asset('royal/images/100935.ico') }}" />
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
  <link href="{{ asset('royal/css/sweetalert.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('royal/css/style.css') }}"/>
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">  
  <link href="https://www.malot.fr/bootstrap-datetimepicker/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css" rel="stylesheet">
  <!-- endinject -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <!--///// jquery para todooo /////////////////////////////////-->    
  <script src="{{ asset('royal/js/sweetalert.min.js') }}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
  <script src="https://www.malot.fr/bootstrap-datetimepicker/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
  <script src="http://code.highcharts.com/highcharts.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>
</head>
<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
        @if(Auth::user()->tipo_user == '1')
        <a class="navbar-brand brand-logo mr-5" href="{{ url('/home') }}"><p style="font-size:30px;color:white;"><strong>V</strong>ary<strong>S</strong>al</p></a>
        <a class="navbar-brand brand-logo-mini" href="{{ url('/home') }}"><img src="{{ asset('royal/images/faces/photo-machine.png') }}" alt="logo"/></a>
        @else
        <a class="navbar-brand brand-logo mr-5" href="{{ url('/citas') }}"><p style="font-size:30px;color:white;"><strong>V</strong>ary<strong>S</strong>al</p></a>
        <a class="navbar-brand brand-logo-mini" href="{{ url('/citas') }}"><img src="{{ asset('royal/images/faces/photo-machine.png') }}" alt="logo"/></a>
        @endif
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
        <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
          <span class="ti-view-list"></span>
        </button>

        <ul class="navbar-nav navbar-nav-right">

          <li class="nav-item nav-profile dropdown">
            <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
              <img src="{{ asset('royal/images/faces/photo-machine.png') }}" alt="profile"/>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">

              <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                <i class="ti-power-off text-primary"></i>
                Cerrar Sesiòn
              </a>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
              </form>
            </div>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="ti-view-list"></span>
        </button>
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          @if(Auth::user()->tipo_user == '1')

          <li class="nav-item">
            <a class="nav-link" href="{{ url('home') }}">
              <i class="ti-home  menu-icon"></i>
              <span class="menu-title">Inicio</span>
            </a>
          </li>
          @endif
          <!--<li class="nav-item">
            <a class="nav-link" href="{{ url('cumpleanos') }}">
              <i class="ti-camera  menu-icon"></i>
              <span class="menu-title">Cumpleaños</span>
            </a>
          </li>-->
          <li class="nav-item">
            <a class="nav-link" href="{{ url('citas') }}">
              <i class="ti-camera  menu-icon"></i>
              <span class="menu-title">Citas</span>
            </a>
          </li>
          <li class="nav-item"> 
            <a class="nav-link" href="{{ url('mensajes') }}" >
               <i class="ti-email  menu-icon"></i>
              <span class="menu-title">Mensajes</span>
            </a>
          </li>
          <li class="nav-item"> 
            <a class="nav-link" href="{{ url('mails') }}" >
               <i class="ti-comment-alt   menu-icon"></i>
              <span class="menu-title">Emails</span>
            </a>
          </li>
          @if(Auth::user()->tipo_user == '2')
          <li class="nav-item"> 
            <a class="nav-link" href="{{ url('clientes') }}" >
               <i class="ti-user   menu-icon"></i>
              <span class="menu-title">Clientes</span>
            </a>
          </li>
          @endif
          @if(Auth::user()->tipo_user == '1')
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
              <i class="ti-settings  menu-icon"></i>
              <span class="menu-title">Configuración</span>
              <i class="menu-arrow"></i>

            </a>
            <div class="collapse" id="ui-basic">
              <ul class="nav flex-column sub-menu">
                
                <li class="nav-item"> <a class="nav-link" href="{{ url('ubicaciones') }}">Ubicacion</a></li>
                
                <li class="nav-item"> <a class="nav-link" href="{{ url('clientes') }}">Clientes</a></li>
                
                <li class="nav-item"> <a class="nav-link" href="{{ url('paquetes') }}">Paquetes</a></li>

                <li class="nav-item"> <a class="nav-link" href="{{ url('usuarios') }}">Usuarios</a></li>
              </ul>
            </div>
          </li>
          @endif
        </ul>
      </nav>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          @yield('content')
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © <?php echo date('Y'); ?> <a href="" target="_blank">VarySal</a>. Todos los Derechos Reservados.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Version 1.0 </span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <script src="{{ asset('royal/vendors/base/vendor.bundle.base.js') }}"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <script src="{{ asset('royal/vendors/chart.js/Chart.min.js') }}"></script>
  <!-- End plugin js for this page-->
  <!-- inject:js -->
 
  <script src="{{ asset('royal/js/off-canvas.js') }}"></script>
  <script src="{{ asset('royal/js/hoverable-collapse.js') }}"></script>
  <script src="{{ asset('royal/js/template.js') }}"></script>
  <script src="{{ asset('royal/js/todolist.js') }}"></script>

  <!-- endinject -->
  <!-- Custom js for this page-->
  
  <!-- End custom js for this page-->
</body>

</html>

