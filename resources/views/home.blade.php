@extends('layouts.inicio')

@section('content')

    <div class="row">
    <div class="col-md-3 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <p class="card-title text-md-center text-xl-left">Venta Paquete Básico</p>
          <div class="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">
            <h3 class="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0"><?php echo $total = $citas_contadas_basico * 400; ?></h3>
            <i class="ti-money icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
          </div>  
          <!--<p class="mb-0 mt-2 text-danger">0.12% <span class="text-black ml-1"><small>(30 days)</small></span></p>-->
        </div>
      </div>
    </div>
    <div class="col-md-3 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <p class="card-title text-md-center text-xl-left">Venta Paquete Gold</p>
          <div class="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">
            <h3 class="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0"><?php echo $total = $citas_contadas_gold * 800; ?></h3>
            <i class="ti-money icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
          </div>  
          <!--<p class="mb-0 mt-2 text-danger">0.12% <span class="text-black ml-1"><small>(30 days)</small></span></p>-->
        </div>

      </div>
    </div>
    <div class="col-md-3 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <p class="card-title text-md-center text-xl-left">Clientes</p>
          <div class="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">
            <h3 class="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0">{{ $contar_cliente }}</h3>
            <i class="ti-user icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
          </div>  
          <!--<p class="mb-0 mt-2 text-danger">0.47% <span class="text-black ml-1"><small>(30 days)</small></span></p>-->
        </div>
        <a class="btn btn-warning btn-fw" href="{{ url('exports/excel/cliente') }}" target="_blank" ><i class="fas fa-file-excel"></i> Reporte EXCEL</a>
      </div>
    </div>
    <div class="col-md-3 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <p class="card-title text-md-center text-xl-left">Sesiones</p>
          <div class="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">
            <h3 class="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0">{{ $citas_contadas }}</h3>
            <i class="ti-gallery icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
          </div>  
          <!--<p class="mb-0 mt-2 text-success">64.00%<span class="text-black ml-1"><small>(30 days)</small></span></p>-->
        </div>
      </div>
    </div>
    <!--<div class="col-md-3 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <p class="card-title text-md-center text-xl-left">Returns</p>
          <div class="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">
            <h3 class="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0">61344</h3>
            <i class="ti-layers-alt icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
          </div>  
          <p class="mb-0 mt-2 text-success">23.00%<span class="text-black ml-1"><small>(30 days)</small></span></p>
        </div>
      </div>
    </div>-->
    </div>

    <div class="row">
       <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
            <p class="card-title">Sesiones del Año <?php echo date('Y'); ?></p>
            <div id="sales-legend" class="chartjs-legend mt-4 mb-2">
              <ul class="1-legend">
                <li><span style="background-color:#0f607a"></span>Offline Sales</li>
                <br>
                <li><span style="background-color:#b7b015"></span>Online Sales</li>
              </ul>
            </div>
            <canvas id="sales-chart" style="display: block; width: 491px; height: 245px;" width="491" height="245" class="chartjs-render-monitor"></canvas>
          </div>

        </div>
      </div>
    </div>
    

    <div class="row">
    <div class="col-md-12 grid-margin stretch-card">
      <div class="card position-relative">
        <div class="card-body">
          <p class="card-title">Reporte de Ubicación</p>
          <div class="row">
            <div class="col-md-12 col-xl-3 d-flex flex-column justify-content-center">
              <div class="ml-xl-4">
                <h1>{{ $contar_ubicacion }}</h1>
                <h3 class="font-weight-light mb-xl-4">Visitas</h3>
                <p class="text-muted mb-2 mb-xl-0">El total de Visitas en las ubicaciones que existen en Ciudad Victoria en las cuales
                hemos estado Fotografiando</p>
              </div>  
            </div>
            <div class="col-md-12 col-xl-9">
              <div class="row">
                <div class="col-md-6 mt-3 col-xl-5">
                  <a class="btn btn-warning btn-fw" href="{{ url('exports/excel/gold') }}" target="_blank" ><i class="fas fa-file-excel"></i> Reporte EXCEL</a>
                </div>
                <div class="col-md-6 col-xl-7">
                  <div class="table-responsive mb-3 mb-md-0">
                    <table class="table table-borderless report-table">
                      @foreach ($contar as $contar_er)
                      <tr>
                        
                        <td class="text-muted"><i class="ti-location-arrow"></i> {{ $contar_er->nombre_ubicacion }}</td>
                        
                        <td class="w-100 px-0">
                          <div class="progress progress-md mx-4">
                            <div class="progress-bar bg-primary" role="progressbar" style="width: 2%" aria-valuenow="2" aria-valuemin="0" aria-valuemax="100"></div>
                          </div>
                        </td>
                        <td><h5 class="font-weight-bold mb-0">{{ $contar_er->contar }}</h5></td>

                      </tr>
                      @endforeach
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

     
    </div>


<!--/////////////////// meses de evento basico ////////////////////////-->
@foreach ($enero_basicos as $basico_enero)
 <?php  $enero_basico = $basico_enero->enero_basico; ?>
@endforeach
@foreach ($febrero_basicos as $basico_febrero)
<?php  $febrero_basico = $basico_febrero->febrero_basico; ?>
@endforeach
@foreach ($marzo_basicos as $basico_marzo)
<?php  $marzo_basico = $basico_marzo->marzo_basico; ?>
@endforeach
@foreach ($abril_basicos as $basico_abril)
<?php  $abril_basico = $basico_abril->abril_basico; ?>
@endforeach
@foreach ($mayo_basicos as $basico_mayo)
<?php  $mayo_basico = $basico_mayo->mayo_basico; ?>
@endforeach
@foreach ($junio_basicos as $basico_junio)
<?php  $junio_basico = $basico_junio->junio_basico; ?>
@endforeach
@foreach ($julio_basicos as $basico_julio)
<?php  $julio_basico = $basico_julio->julio_basico; ?>
@endforeach
@foreach ($agosto_basicos as $basico_agosto)
<?php  $agosto_basico = $basico_agosto->agosto_basico; ?>
@endforeach
@foreach ($septiembre_basicos as $basico_septiembre)
<?php  $septiembre_basico = $basico_septiembre->septiembre_basico; ?>
@endforeach
@foreach ($octubre_basicos as $basico_octubre)
<?php  $octubre_basico = $basico_octubre->octubre_basico; ?>
@endforeach
@foreach ($noviembre_basicos as $basico_noviembre)
<?php  $noviembre_basico = $basico_noviembre->noviembre_basico; ?>
@endforeach
@foreach ($diciembre_basicos as $basico_diciembre)
<?php  $diciembre_basico = $basico_diciembre->diciembre_basico; ?>
@endforeach
<!--//////////////////////// valores //////////////////////////////////-->
<?php  $meses_basico = $enero_basico.','.$febrero_basico.','.$marzo_basico.','.$abril_basico.','.$mayo_basico.','.$junio_basico.','.$julio_basico.','.$agosto_basico.','.$septiembre_basico.','.$octubre_basico.','.$noviembre_basico.','.$diciembre_basico; ?>
<!--//////////////////////// FIN //////////////////////////////////-->
<!--/////////////////// meses de evento basico ////////////////////////-->
@foreach ($enero_golds as $gold_enero)
 <?php  $enero_gold = $gold_enero->enero_gold; ?>
@endforeach
@foreach ($febrero_golds as $gold_febrero)
<?php  $febrero_gold = $gold_febrero->febrero_gold; ?>
@endforeach
@foreach ($marzo_golds as $gold_marzo)
<?php  $marzo_gold = $gold_marzo->marzo_gold; ?>
@endforeach
@foreach ($abril_golds as $gold_abril)
<?php  $abril_gold = $gold_abril->abril_gold; ?>
@endforeach
@foreach ($mayo_golds as $gold_mayo)
<?php  $mayo_gold = $gold_mayo->mayo_gold; ?>
@endforeach
@foreach ($junio_golds as $gold_junio)
<?php  $junio_gold = $gold_junio->junio_gold; ?>
@endforeach
@foreach ($julio_golds as $gold_julio)
<?php  $julio_gold = $gold_julio->julio_gold; ?>
@endforeach
@foreach ($agosto_golds as $gold_agosto)
<?php  $agosto_gold = $gold_agosto->agosto_gold; ?>
@endforeach
@foreach ($septiembre_golds as $gold_septiembre)
<?php  $septiembre_gold = $gold_septiembre->septiembre_gold; ?>
@endforeach
@foreach ($octubre_golds as $gold_octubre)
<?php  $octubre_gold = $gold_octubre->octubre_gold; ?>
@endforeach
@foreach ($noviembre_golds as $gold_noviembre)
<?php  $noviembre_gold = $gold_noviembre->noviembre_gold; ?>
@endforeach
@foreach ($diciembre_golds as $gold_diciembre)
<?php  $diciembre_gold = $basico_diciembre->diciembre_basico; ?>
@endforeach
<!--//////////////////////// valores //////////////////////////////////-->
<?php  $meses_gold = $enero_gold.','.$febrero_gold.','.$marzo_gold.','.$abril_gold.','.$mayo_gold.','.$junio_gold.','.$julio_gold.','.$agosto_gold.','.$septiembre_gold.','.$octubre_gold.','.$noviembre_gold.','.$diciembre_gold; ?>
<!--//////////////////////// FIN //////////////////////////////////-->
  <script>
    (function($) {
  'use strict';
  $(function() {
    if ($("#order-chart").length) {
      var areaData = {
        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul"],
        datasets: [
          {
            data: [175, 200, 130, 210, 40, 60, 25],
            backgroundColor: [
              'rgba(255, 193, 2, .8)'
            ],
            borderColor: [
              'transparent'
            ],
            borderWidth:3,
            fill: 'origin',
            label: "services"
          },
          {
            data: [175, 145, 190, 130, 240, 160, 200],
            backgroundColor: [
              'rgba(245, 166, 35, 1)'
            ],
            borderColor: [
              'transparent'
            ],
            borderWidth:3,
            fill: 'origin',
            label: "purchases"
          }
        ]
      };
      var areaOptions = {
        responsive: true,
        maintainAspectRatio: true,
        plugins: {
          filler: {
            propagate: false
          }
        },
        scales: {
          xAxes: [{
            display: false,
            ticks: {
              display: true
            },
            gridLines: {
              display: false,
              drawBorder: false,
              color: 'transparent',
              zeroLineColor: '#eeeeee'
            }
          }],
          yAxes: [{
            display: false,
            ticks: {
              display: true,
              autoSkip: false,
              maxRotation: 0,
              stepSize: 100,
              min: 0,
              max: 260
            },
            gridLines: {
              drawBorder: false
            }
          }]
        },
        legend: {
          display: false
        },
        tooltips: {
          enabled: true
        },
        elements: {
          line: {
            tension: .45
          },
          point: {
            radius: 0
          }
        }
      }
      var salesChartCanvas = $("#order-chart").get(0).getContext("2d");
      var salesChart = new Chart(salesChartCanvas, {
        type: 'line',
        data: areaData,
        options: areaOptions
      });
    }

    if ($("#sales-chart").length) {
      var SalesChartCanvas = $("#sales-chart").get(0).getContext("2d");
      var SalesChart = new Chart(SalesChartCanvas, {
        type: 'bar',
        data: {
          labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
          datasets: [{
              label: 'Sesiones Básica',
              data: [<?php echo $meses_basico; ?>],
              backgroundColor: '#0f607a'
            },
            {
              label: 'Sesiones Gold',
              data: [<?php echo $meses_gold; ?>],
              backgroundColor: '#b7b015'
            }
          ]
        },
        options: {
          responsive: true,
          maintainAspectRatio: true,
          layout: {
            padding: {
              left: 0,
              right: 0,
              top: 20,
              bottom: 0
            }
          },
          scales: {
            yAxes: [{
              display: true,
              gridLines: {
                display: false,
                drawBorder: false
              },
              ticks: {
                display: false,
                min: 0,
                max: 15
              }
            }],
            xAxes: [{
              stacked: false,
              ticks: {
                beginAtZero: true,
                fontColor: "#9fa0a2"
              },
              gridLines: {
                color: "rgba(0, 0, 0, 0)",
                display: false
              },
              barPercentage: 1
            }]
          },
          legend: {
            display: false
          },
          elements: {
            point: {
              radius: 0
            }
          }
        },
      });
      document.getElementById('sales-legend').innerHTML = SalesChart.generateLegend();
    }

    if ($("#north-america-chart").length) {
      var areaData = {
        labels: ["Jan", "Feb", "Mar"],
        datasets: [{
            data: [100, 50, 50],
            backgroundColor: [
              "#71c016", "#8caaff", "#248afd",
            ],
            borderColor: "rgba(0,0,0,0)"
          }
        ]
      };
      var areaOptions = {
        responsive: true,
        maintainAspectRatio: true,
        segmentShowStroke: false,
        cutoutPercentage: 78,
        elements: {
          arc: {
              borderWidth: 4
          }
        },      
        legend: {
          display: false
        },
        tooltips: {
          enabled: true
        },
        legendCallback: function(chart) { 
          var text = [];
          text.push('<div class="report-chart">');
            text.push('<div class="d-flex justify-content-between mx-4 mx-xl-5 mt-3"><div class="d-flex align-items-center"><div class="mr-3" style="width:20px; height:20px; border-radius: 50%; background-color: ' + chart.data.datasets[0].backgroundColor[0] + '"></div><p class="mb-0">Offline sales</p></div>');
            text.push('<p class="mb-0">22789</p>');
            text.push('</div>');
            text.push('<div class="d-flex justify-content-between mx-4 mx-xl-5 mt-3"><div class="d-flex align-items-center"><div class="mr-3" style="width:20px; height:20px; border-radius: 50%; background-color: ' + chart.data.datasets[0].backgroundColor[1] + '"></div><p class="mb-0">Online sales</p></div>');
            text.push('<p class="mb-0">94678</p>');
            text.push('</div>');
            text.push('<div class="d-flex justify-content-between mx-4 mx-xl-5 mt-3"><div class="d-flex align-items-center"><div class="mr-3" style="width:20px; height:20px; border-radius: 50%; background-color: ' + chart.data.datasets[0].backgroundColor[2] + '"></div><p class="mb-0">Returns</p></div>');
            text.push('<p class="mb-0">12097</p>');
            text.push('</div>');
          text.push('</div>');
          return text.join("");
        },
      }
      var northAmericaChartPlugins = {
        beforeDraw: function(chart) {
          var width = chart.chart.width,
              height = chart.chart.height,
              ctx = chart.chart.ctx;
      
          ctx.restore();
          var fontSize = 3.125;
          ctx.font = "600 " + fontSize + "em sans-serif";
          ctx.textBaseline = "middle";
          ctx.fillStyle = "#000";
      
          var text = "63",
              textX = Math.round((width - ctx.measureText(text).width) / 2),
              textY = height / 2;
      
          ctx.fillText(text, textX, textY);
          ctx.save();
        }
      }
      var northAmericaChartCanvas = $("#north-america-chart").get(0).getContext("2d");
      var northAmericaChart = new Chart(northAmericaChartCanvas, {
        type: 'doughnut',
        data: areaData,
        options: areaOptions,
        plugins: northAmericaChartPlugins
      });
      document.getElementById('north-america-legend').innerHTML = northAmericaChart.generateLegend();
    }

  });
})(jQuery);
  </script>

 
@endsection
