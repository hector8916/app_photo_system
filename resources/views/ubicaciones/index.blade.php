@extends('layouts.inicio')

@section('content')
<div class="col-md-12">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Ubicaciones</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="row">
        <div class="col-md-12">
         <a class="btn btn-primary btn-sm" href="{{ route('ubicaciones.create') }}"><i class="ti-plus menu-icon"></i> Nueva Ubicacion</a>
         <div style="height: 5px;"></div>
         <div class="card">
                <div class="card-body">
                  
                  <div class="table-responsive">
                    <table class="table">
			          <thead>
			            <tr>
			             <th>Nombre de la Ubicación</th>
			             <th>Direccion</th>
			             <th>Codigo Postal</th>
			              <th >Acciones</th>
			            </tr>
			          </thead>
			          <tbody>
			            @foreach ($ubicaciones as $ubicacion)
			            <tr>
			                <td>{{ $ubicacion->nombre_ubicacion }}</td>
			                <td>{{ $ubicacion->direccion }}</td>
			                <td>{{ $ubicacion->cp }}</td>
			                <td>

			                	<div class="dropdown">
								  <button class="btn btn-link dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								    <i class="fas fa-bars"></i>
								  </button>
								  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
								    <a class="dropdown-item" href="{{ route('ubicaciones.edit',$ubicacion->id)}}"><i class="ti-pencil  menu-icon"></i> Editar</a>
								    
								    <form action="{{ route('ubicaciones.destroy',$ubicacion->id )}}" method="POST">

			                        @csrf
			                        @method('DELETE')
								    <button class="dropdown-item" type="submit"><i class="ti-trash  menu-icon"></i> Eliminar</button>
								    </form>
								  </div>
								</div>

			                </td>
			            </tr>
			            @endforeach
			          </tbody>
			        </table>
			        {!! $ubicaciones->links() !!}
                  </div>
                </div>
              </div>
        
        </div>
      </div>
      <!-- /.row -->
    </div>
  </div>
  <!-- /.box -->
</div>


@endsection