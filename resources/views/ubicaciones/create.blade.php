@extends('layouts.inicio')

@section('content')

<div class="col-12 grid-margin">
  <div class="card">
    <div class="card-body">
      <form class="form-sample">
        <p class="card-description">
          Informacion de Ubicación
        </p>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Nombre de la Ubicación</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" value="@isset($ubicacion){{ $ubicacion->nombre_ubicacion }}@endisset" name="nombre_ubicacion"  placeholder="Ingresa el nombre de la ubicacion)" >
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Direccion</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" value="@isset($ubicacion){{ $ubicacion->direccion }}@endisset" name="direccion" placeholder="Ingresa la direccion">
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Codigo Postal</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" value="@isset($ubicacion){{ $ubicacion->cp }}@endisset" name="cp" placeholder="Ingresa el codigo postal">
              </div>
            </div>
          </div>

        </div>
        <div class="row">
          <div class="col-md-6">
          	<a href="{{ url('ubicaciones') }}" class="btn btn-inverse-secondary btn-fw"><i class="ti-back-left  menu-icon"></i> Regresar</a>
            <?php 

              if (@isset($ubicacion) == true) { ?>
               <button type="submit" class="btn btn-inverse-primary btn-fw btn-submit"><i class="ti-save  menu-icon"></i> Editar</button>
            <?php }else{ ?>
              <button type="submit" class="btn btn-inverse-primary btn-fw btn-submit"><i class="ti-save  menu-icon"></i> Guardar</button>
            <?php  }
            ?>
          </div>
        
        </div>
      </form>
    </div>
  </div>
</div>


<script type="text/javascript">
    $(".btn-submit").click(function(e){

        e.preventDefault();

        var nombre_ubicacion = $("input[name=nombre_ubicacion]").val();

        var direccion = $("input[name=direccion]").val();

        var cp = $("input[name=cp]").val();



        $.ajax({

           type:"{{ ( isset($ubicacion) ? 'PUT' : 'POST' ) }}",

           url:"{{ ( isset($ubicacion) ) ? '/ubicaciones/' . $ubicacion->id : '/ubicaciones/create' }}",
           headers: {
			      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			     },
           data:{
           	nombre_ubicacion:nombre_ubicacion, 
           	direccion:direccion,
           	cp:cp, 
           },
           
            success:function(data){
                swal({title: "Felicidades!", text: data.success, type: "success"}, function(){ location.href ="{{ url('ubicaciones') }}"; } );
            }


        });



  });

</script>
@endsection