@extends('layouts.inicio')

@section('content')
<div class="col-md-12">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Mensajes Recibidos</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="row">
        <div class="col-md-12">
         
         <div class="card">
                <div class="card-body">
                  
                  <div class="table-responsive">
                    <table class="table">
			          <thead>
			            <tr>
			             <th>Nombre del cliente</th>
			             <th>Correo Electronico</th>
			             <th>Asunto</th>
			             <th>Fecha Recibido</th>
			              <th >Acciones</th>
			            </tr>
			          </thead>
			          <tbody>
			            @foreach ($mensajes as $mensaje)
			            <tr>
			                <td>{{ $mensaje->nombre_cliente }}</td>
			                <td>{{ $mensaje->correo_electronico }}</td>
			                <td>{{ $mensaje->aunto }}</td>
			                <td>{{ $mensaje->created_at }}</td>
			                
			                <td>

			                	<div class="dropdown">
								  <button class="btn btn-link dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								    <i class="fas fa-bars"></i>
								  </button>
								  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
								    <a class="dropdown-item" href="{{ route('mensajes.show',$mensaje->id)}}"><i class="fas fa-eye"></i> Ver Mensaje</a>
								    
								    <form action="{{ route('mensajes.destroy',$mensaje->id )}}" method="POST">

			                        @csrf
			                        @method('DELETE')
								    <button class="dropdown-item" type="submit"><i class="ti-trash  menu-icon"></i> Eliminar</button>
								    </form>
								  </div>
								</div>


			                </td>
			            </tr>
			            @endforeach
			          </tbody>
			        </table>
			        {!! $mensajes->links() !!}
                  </div>
                </div>
              </div>
        
        </div>
      </div>
      <!-- /.row -->
    </div>
  </div>
  <!-- /.box -->
</div>


@endsection