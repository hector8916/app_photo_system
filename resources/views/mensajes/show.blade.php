@extends('layouts.inicio')

@section('content')
<div class="card">
  <div class="card-body">
    
    <section class="invoice">
        
          <!-- title row -->
          <div class="row">
            <div class="col-xs-12">
              <h3 class="page-header">
                 Mensajes Recibidos.
              </h3>
            </div>
            <!-- /.col -->
          </div>
          <!-- info row -->
          @foreach ($mensajes as $mensaje)
          <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
              Nombre Cliente
              <address>
                <strong>{{ $mensaje->nombre_cliente }}</strong>
                
              </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
              Asunto
              <address>
                <strong>{{ $mensaje->aunto }}</strong><br>
              </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
               Correo Electronico
              <address>
                <strong>{{ $mensaje->correo_electronico }}</strong><br>
              </address>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->

          <!-- Table row -->
          <div class="row">
            <label>Mensaje</label>
            
            <div class="col-xs-12 table-responsive">
              <textarea class="form-control" rows="4">{{ $mensaje->mensaje }}</textarea>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
          @endforeach

          <!-- /.row -->
          <div style="height: 49px;"></div>
          <!-- this row will not appear when printing -->
          <div class="row">
          <div class="col-md-6">
            <a href="{{ url('mensajes') }}" class="btn btn-inverse-secondary btn-fw"><i class="ti-back-left  menu-icon"></i> Regresar</a>
            
          </div>
        
        </div>
         
    </section>
  </div>
</div>

@endsection