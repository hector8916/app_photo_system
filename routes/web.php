<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/////////////////////////////////clientes/////////////////////////
Route::resource('clientes','ClienteController');
Route::post('/clientes/create', 'ClienteController@store');
Route::put('/{id}', 'ClienteController@update');
/////////////////////////////////ubicaciones/////////////////////////
Route::resource('ubicaciones','UbicacionController');
Route::post('/ubicaciones/create', 'UbicacionController@store');
Route::put('/{id}', 'UbicacionController@update');
////////////////////////////////paquetes//////////////////////////
Route::resource('paquetes','PaqueteController');
Route::post('/paquetes/create', 'PaqueteController@store');
Route::put('/{id}', 'PaqueteController@update');

/////////////////////////////////Citas/////////////////////////
Route::resource('citas','CitaController');
Route::post('/citas/create', 'CitaController@store');
Route::put('/{id}', 'CitaController@update');

/////////////////////////////////mensajes/////////////////////////
Route::resource('mensajes','MensajeController');
//////////////////////////////// mails //////////////////////////
Route::get('/mails','MailController@home');
Route::post('send/email','MailController@sendemail');
/////////////////////////////////usuarios/////////////////////////
Route::resource('usuarios','UsuarioController');
Route::post('/usuarios/create', 'UsuarioController@store');
Route::put('/{id}', 'UsuarioController@update');
/////////////////////////////////cumpleanos/////////////////////////
Route::resource('cumpleanos','CumpleController');
/////////////  exportaciones /////////////////////////////////////
Route::get('exports/excel/gold', 'ExcelController@GoldExcel');
Route::get('exports/excel/cliente', 'ExcelController@ClienteExcel');