<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Citas extends Model
{
	protected $hidden = ['created_at','updated_at'];
    protected $fillable = ['id','num_cliente ','num_paquete','num_ubicacion','fecha_cita','estatus_cita','activo'];
    protected $table = 'citas';


      public function _NombreCliente(){
	    return $this->hasOne('App\Clientes', 'id', 'num_cliente');
	  }

	  public function _Paquete(){
	    return $this->hasOne('App\Paquetes', 'id', 'num_paquete');
	  }

	  public function _Ubicacion(){
	    return $this->hasOne('App\Ubicaciones', 'id', 'num_ubicacion');
	  }

	  public function _Estatus(){
	    return $this->hasOne('App\EstatusCita', 'id', 'estatus_cita');
	  }

}