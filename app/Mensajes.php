<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mensajes extends Model
{
	protected $hidden = ['created_at','updated_at'];
    protected $fillable = ['id','nombre_cliente','correo_electronico','asunto','mensaje','activo'];
    protected $table = 'mensajes';

    
}