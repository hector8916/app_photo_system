<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstatusCita extends Model
{
	protected $hidden = ['created_at','updated_at'];
    protected $fillable = ['id','nombre_estatus_cita'];
    protected $table = 'estatus_cita';

    
}