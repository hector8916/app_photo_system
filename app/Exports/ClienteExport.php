<?php 

namespace App\Exports;

use App\Clientes;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;



class ClienteExport implements FromCollection
{
    public function collection()
    {
        
    	return Clientes::select('nombre_cliente','apellido_paterno','apellido_materno','fecha_nacimiento','direccion','correo_electronico','telefono_celular')->get();
    }

    
}