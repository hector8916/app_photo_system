<?php 

namespace App\Exports;

use App\Citas;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;



class CitaExport implements FromCollection
{
    public function collection()
    {
        
    	return Citas::all();
    }

    
}