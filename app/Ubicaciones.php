<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ubicaciones extends Model
{
	protected $hidden = ['created_at','updated_at'];
    protected $fillable = ['id','nombre_ubicacion','direccion','cp','activo'];
    protected $table = 'ubicacion';

    
}