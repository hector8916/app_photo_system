<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Mail;
use Session;
use App\Mail\SendEmail;
use App\Clientes;

class MailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function home(){

      $datos['pacientes'] = Clientes::where([
            ['activo', '=', '1'],
            ['correo_electronico', '!=', NULL],
        ])->orderBy('id')->get();
      
      return view('mails.index')->with($datos);
        
    }
     public function sendemail(Request $get){
        $this->validate($get,[
        	"email" => 'required',
        	"subject" => 'required',
        	"message" => 'required',
        ]);
        $email = $get->email;
        $subject = $get->subject;
        $message = $get->message;

        Mail::to($email)->send( new SendEmail($subject,$message ));

        Session::flash('success');
        return back();
    }







}
