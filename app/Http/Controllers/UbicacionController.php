<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Ubicaciones;





class UbicacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

       $ubicaciones = Ubicaciones::where('activo','1')->orderBy('id')->latest()->paginate(5);

        return view('ubicaciones.index', compact('ubicaciones'))->with('i',(request()->input('page',1)-1)*5);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('ubicaciones.create');        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            try {
                $cliente = new Ubicaciones();
                $cliente->nombre_ubicacion = $request->nombre_ubicacion;
                $cliente->direccion = $request->direccion;
                $cliente->cp = $request->cp;
                $cliente->save();
                return response()->json(['success'=>'Se ha Agegado con Exito']);
            } catch (\Exception $e) {
              dd($e->getMessage());
            }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
     public function show()
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $datos['ubicacion'] = Ubicaciones::find($id);

        
        return view('ubicaciones.create')->with($datos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {

        try {
            $ubicaciones = Ubicaciones::find($id);
            $ubicaciones->fill($request->all());
            $ubicaciones->save();

        
           return response()->json(['success'=>'Se ha Editado con Exito']);

        } catch (\Exception $e) {
          dd($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ubicaciones = Ubicaciones::find($id);
        $ubicaciones->activo = 0;
        $ubicaciones->save();
        
       

        return redirect()->route('ubicaciones.index');
    }






}
