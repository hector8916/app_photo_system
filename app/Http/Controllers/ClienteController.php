<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Clientes;
use App\TipoSexo;




class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

       $clientes = Clientes::where('activo','1')->orderBy('id')->latest()->paginate(10);

        return view('clientes.index', compact('clientes'))->with('i',(request()->input('page',1)-1)*5);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $datos['sexo'] = TipoSexo::where([
                                  ['activo', '=', '1'],
                              ])->orderBy('id')->get();
        return view('clientes.create')->with($datos);        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            try {
                $cliente = new Clientes();
                $cliente->nombre_cliente = $request->nombre_cliente;
                $cliente->apellido_paterno = $request->apellido_paterno;
                $cliente->apellido_materno = $request->apellido_materno;
                $cliente->direccion = $request->direccion;
                $cliente->cp = $request->cp;
                $cliente->fecha_nacimiento = $request->fecha_nacimiento;
                $cliente->sexo = $request->sexo;
                $cliente->correo_electronico = $request->correo_electronico;
                $cliente->telefono = $request->telefono;
                $cliente->telefono_celular = $request->telefono_celular;
                $cliente->save();
                return response()->json(['success'=>'Se ha Agegado con Exito']);
            } catch (\Exception $e) {
              dd($e->getMessage());
            }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
     public function show()
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $datos['cliente'] = Clientes::find($id);

        $datos['sexo'] = TipoSexo::where([
                                  ['activo', '=', '1'],
                              ])->orderBy('id')->get();
        return view('clientes.create')->with($datos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {

        try {
            $clientes = Clientes::find($id);
            $clientes->fill($request->all());
            $clientes->save();

        
           return response()->json(['success'=>'Se ha Editado con Exito']);

        } catch (\Exception $e) {
          dd($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $clientes = Clientes::find($id);
        $clientes->activo = 0;
        $clientes->save();
        
       

        return redirect()->route('clientes.index');
    }






}
