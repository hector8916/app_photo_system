<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Clientes;
use App\TipoSexo;
use Carbon\Carbon;

class CumpleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $cumpleanos = Clientes::where('activo','1')->get();
       foreach ($cumpleanos as $key => $value) {
           
        list($fecha,$hora) = explode(' ', $value['fecha_nacimiento']);
        list($anio,$mes,$dia) = explode('-', $fecha);
        $fechas = $mes.'-'.$dia;
        $fecha_actual = date('m-d');

        $date = Carbon::createFromDate($anio,$dia,$mes)->age;

        

        return view('cumpleanos.index');

       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
            
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
     public function show()
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {

       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }






}
