<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;





class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

       $usuarios = User::where('activo','1')->orderBy('id')->latest()->paginate(5);

        return view('usuarios.index', compact('usuarios'))->with('i',(request()->input('page',1)-1)*5);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('usuarios.create');        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            try {
                $usuario = new User();
                $usuario->name = $request->name;
                $usuario->name_user = $request->name_user;
                $usuario->first_user = $request->first_user;
                $usuario->email = $request->email;
                $usuario->password = bcrypt($request->password);
                $usuario->tipo_user = $request->tipo_user;
                $usuario->save();
                return response()->json(['success'=>'Se ha Agegado con Exito']);
            } catch (\Exception $e) {
              dd($e->getMessage());
            }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
     public function show()
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $datos['usuario'] = User::find($id);

        
        return view('usuarios.create')->with($datos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {

        try {
            $usuarios = User::find($id);
            $usuarios->fill($request->all());
            $usuarios->save();

        
           return response()->json(['success'=>'Se ha Editado con Exito']);

        } catch (\Exception $e) {
          dd($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuarios = User::find($id);
        $usuarios->activo = 0;
        $usuarios->save();
        
       

        return redirect()->route('usuarios.index');
    }






}
