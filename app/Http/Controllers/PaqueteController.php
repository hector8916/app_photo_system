<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Paquetes;




class PaqueteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

       $paquetes = Paquetes::where('activo','1')->orderBy('id')->latest()->paginate(10);

        return view('paquetes.index', compact('paquetes'))->with('i',(request()->input('page',1)-1)*5);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('paquetes.create');        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            try {
                $cliente = new Paquetes();
                $cliente->nombre_paquete = $request->nombre_paquete;
                $cliente->precio_paquete = $request->precio_paquete;
                $cliente->descripcion_paquete = $request->descripcion_paquete;
                $cliente->save();
                return response()->json(['success'=>'Se ha Agegado con Exito']);
            } catch (\Exception $e) {
              dd($e->getMessage());
            }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
     public function show()
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $datos['paquete'] = Paquetes::find($id);
        return view('paquetes.create')->with($datos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {

        try {
            $paquetes = Paquetes::find($id);
            $paquetes->fill($request->all());
            $paquetes->save();

        
           return response()->json(['success'=>'Se ha Editado con Exito']);

        } catch (\Exception $e) {
          dd($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $paquetes = Paquetes::find($id);
        $paquetes->activo = 0;
        $paquetes->save();
        
       

        return redirect()->route('paquetes.index');
    }






}
