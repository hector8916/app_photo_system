<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\CitaExport;
use App\Exports\ClienteExport;


class ExcelController extends Controller
{
   

     public function GoldExcel(){

       return Excel::download(new CitaExport, 'Gold.xlsx');
 
    }

    public function ClienteExcel(){

       return Excel::download(new ClienteExport, 'Clientes.xlsx');
 
    }
}