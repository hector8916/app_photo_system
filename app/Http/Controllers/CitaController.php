<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Citas;
use App\Ubicaciones;
use App\Paquetes;
use App\Clientes;
use App\EstatusCita;
use Illuminate\Support\Facades\DB;



class CitaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

       $citas = Citas::where([['activo','1'],['estatus_cita','1']])->orwhere([['activo','1'],['estatus_cita','2']])->orderBy('fecha_cita','DESC')->latest()->paginate(10);

        return view('citas.index', compact('citas'))->with('i',(request()->input('page',1)-1)*5);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $datos['ubicacion'] = Ubicaciones::where([
                                  ['activo', '=', '1'],
                              ])->orderBy('id')->get();
        $datos['paquete'] = Paquetes::where([
                                  ['activo', '=', '1'],
                              ])->orderBy('id')->get();
        $datos['cliente'] = Clientes::where([
                                  ['activo', '=', '1'],
                              ])->orderBy('id')->get();
        $datos['estatus'] = EstatusCita::orderBy('id')->get();


        $mes = date('m');
        $anio = date('Y');
        $numero = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));

     

       $datos['fecha'] = DB::select("SELECT * FROM citas WHERE fecha_cita BETWEEN '".$anio."-".$mes."-01' and '".$anio."-".$mes."-".$numero."' AND estatus_cita = '1' OR estatus_cita = '2' order by fecha_cita DESC ");



        return view('citas.create')->with($datos);        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            try {
                $cliente = new Citas();
                $cliente->num_cliente = $request->num_cliente;
                $cliente->num_paquete = $request->num_paquete;
                $cliente->num_ubicacion = $request->num_ubicacion;
                $cliente->fecha_cita = $request->fecha_cita;
                $cliente->estatus_cita = '1';
                
                $cliente->save();
                return response()->json(['success'=>'Se ha Agegado con Exito']);
            } catch (\Exception $e) {
              dd($e->getMessage());
            }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
     public function show($id)
    {
        $cita = Citas::find($id);

        $datos['citas'] = Citas::find($id);
        $datos['cliente_id'] = Clientes::where([
                                  ['activo', '=', '1'],
                                  ['id', '=', $cita->num_cliente],
                              ])->orderBy('id')->get();

        $datos['ubicacion_id'] = Ubicaciones::where([
                                  ['activo', '=', '1'],
                                  ['id', '=', $cita->num_ubicacion],
                              ])->orderBy('id')->get();
        $datos['paquete'] = Paquetes::where([
                                  ['activo', '=', '1'],
                                  ['id', '=', $cita->num_paquete],
                              ])->orderBy('id')->get();



        return view('citas.show')->with($datos);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $datos['cita'] = Citas::find($id);

        $datos['ubicacion'] = Ubicaciones::where([
                                  ['activo', '=', '1'],
                              ])->orderBy('id')->get();
        $datos['paquete'] = Paquetes::where([
                                  ['activo', '=', '1'],
                              ])->orderBy('id')->get();
        $datos['cliente'] = Clientes::where([
                                  ['activo', '=', '1'],
                              ])->orderBy('id')->get();
        $datos['estatus'] = EstatusCita::orderBy('id')->get();

        $mes = date('m');
        $anio = date('Y');
        $numero = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));

        $datos['fecha'] = DB::select("SELECT * FROM citas WHERE fecha_cita BETWEEN '".$anio."-".$mes."-01' and '".$anio."-".$mes."-".$numero."' AND estatus_cita = '1' order by fecha_cita DESC ");

        return view('citas.create')->with($datos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {

        try {
            $citas = Citas::find($id);

            $citas->fill($request->all());
            $citas->save();

        
           return response()->json(['success'=>'Se ha Editado con Exito']);

        } catch (\Exception $e) {
          dd($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $citas = Citas::find($id);
        $citas->activo = 0;
        $citas->save();
        
       

        return redirect()->route('citas.index');
    }






}
