<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ubicaciones;
use App\Clientes;
use App\Citas;
use Illuminate\Support\Facades\DB;

use App\Charts\PulseChart;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $datos['ubicacion'] = Ubicaciones::where('activo','1')->get();
        $datos['contar_cliente'] = Clientes::where('activo','1')->count();
        $datos['citas_contadas'] = Citas::where([['activo','1'],['estatus_cita','3']])->count();
        $datos['citas_contadas_gold'] = Citas::where([['num_paquete','2'],['estatus_cita','3']])->count();
        $datos['citas_contadas_basico'] = Citas::where([['num_paquete','1'],['estatus_cita','3']])->count();
        
        $datos['contar'] = DB::select("SELECT COUNT(citas.num_ubicacion) as contar,ubicacion.nombre_ubicacion FROM ubicacion 
                                        INNER JOIN citas ON ubicacion.id = citas.num_ubicacion
                                        WHERE citas.activo = '1'
                                        GROUP BY ubicacion.nombre_ubicacion ");
        $datos['contar_ubicacion'] = Citas::where([['activo','1']])->count();

        $mes = date('m');
        $anio = date('Y');
        $numero = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));

        /////////////// Enero //////////////////
        $datos['enero_basicos'] = DB::select("SELECT COUNT(id) as enero_basico FROM citas WHERE fecha_cita BETWEEN '".$anio."-01-01' and '".$anio."-01-31' and num_paquete = '1' ");
        $datos['enero_golds'] = DB::select("SELECT COUNT(id) as enero_gold FROM citas WHERE fecha_cita BETWEEN '".$anio."-01-01' and '".$anio."-01-31' and num_paquete = '2' ");
        ////////////// febrero /////////////////
        $datos['febrero_basicos'] = DB::select("SELECT COUNT(id) as febrero_basico FROM citas WHERE fecha_cita BETWEEN '".$anio."-02-01' and '".$anio."-02-28' and num_paquete = '1' ");
        $datos['febrero_golds'] = DB::select("SELECT COUNT(id) as febrero_gold FROM citas WHERE fecha_cita BETWEEN '".$anio."-02-01' and '".$anio."-02-28' and num_paquete = '2' ");
        ////////////// marzo /////////////////
        $datos['marzo_basicos'] = DB::select("SELECT COUNT(id) as marzo_basico FROM citas WHERE fecha_cita BETWEEN '".$anio."-03-01' and '".$anio."-03-31' and num_paquete = '1' ");
        $datos['marzo_golds'] = DB::select("SELECT COUNT(id) as marzo_gold FROM citas WHERE fecha_cita BETWEEN '".$anio."-03-01' and '".$anio."-03-31' and num_paquete = '2' ");
        ////////////// abril /////////////////
        $datos['abril_basicos'] = DB::select("SELECT COUNT(id) as abril_basico FROM citas WHERE fecha_cita BETWEEN '".$anio."-04-01' and '".$anio."-04-30' and num_paquete = '1' ");
        $datos['abril_golds'] = DB::select("SELECT COUNT(id) as abril_gold FROM citas WHERE fecha_cita BETWEEN '".$anio."-04-01' and '".$anio."-04-30' and num_paquete = '2' ");
        ////////////// mayo /////////////////
        $datos['mayo_basicos'] = DB::select("SELECT COUNT(id) as mayo_basico FROM citas WHERE fecha_cita BETWEEN '".$anio."-05-01' and '".$anio."-05-31' and num_paquete = '1' ");
        $datos['mayo_golds'] = DB::select("SELECT COUNT(id) as mayo_gold FROM citas WHERE fecha_cita BETWEEN '".$anio."-05-01' and '".$anio."-05-31' and num_paquete = '2' ");
        ////////////// junio /////////////////
        $datos['junio_basicos'] = DB::select("SELECT COUNT(id) as junio_basico FROM citas WHERE fecha_cita BETWEEN '".$anio."-06-01' and '".$anio."-06-30' and num_paquete = '1' ");
        $datos['junio_golds'] = DB::select("SELECT COUNT(id) as junio_gold FROM citas WHERE fecha_cita BETWEEN '".$anio."-06-01' and '".$anio."-06-30' and num_paquete = '2' ");
        ////////////// julio /////////////////
        $datos['julio_basicos'] = DB::select("SELECT COUNT(id) as julio_basico FROM citas WHERE fecha_cita BETWEEN '".$anio."-07-01' and '".$anio."-07-31' and num_paquete = '1' ");
        $datos['julio_golds'] = DB::select("SELECT COUNT(id) as julio_gold FROM citas WHERE fecha_cita BETWEEN '".$anio."-07-01' and '".$anio."-07-31' and num_paquete = '2' ");
        ////////////// agosto /////////////////
        $datos['agosto_basicos'] = DB::select("SELECT COUNT(id) as agosto_basico FROM citas WHERE fecha_cita BETWEEN '".$anio."-08-01' and '".$anio."-08-31' and num_paquete = '1' ");
        $datos['agosto_golds'] = DB::select("SELECT COUNT(id) as agosto_gold FROM citas WHERE fecha_cita BETWEEN '".$anio."-08-01' and '".$anio."-08-31' and num_paquete = '2' ");
        ////////////// septiembre /////////////////
        $datos['septiembre_basicos'] = DB::select("SELECT COUNT(id) as septiembre_basico FROM citas WHERE fecha_cita BETWEEN '".$anio."-09-01' and '".$anio."-09-30' and num_paquete = '1' ");
        $datos['septiembre_golds'] = DB::select("SELECT COUNT(id) as septiembre_gold FROM citas WHERE fecha_cita BETWEEN '".$anio."-09-01' and '".$anio."-09-30' and num_paquete = '2' ");
        ////////////// octubre /////////////////
        $datos['octubre_basicos'] = DB::select("SELECT COUNT(id) as octubre_basico FROM citas WHERE fecha_cita BETWEEN '".$anio."-10-01' and '".$anio."-10-31' and num_paquete = '1' ");
        $datos['octubre_golds'] = DB::select("SELECT COUNT(id) as octubre_gold FROM citas WHERE fecha_cita BETWEEN '".$anio."-10-01' and '".$anio."-10-31' and num_paquete = '2' ");
        ////////////// noviembre /////////////////
        $datos['noviembre_basicos'] = DB::select("SELECT COUNT(id) as noviembre_basico FROM citas WHERE fecha_cita BETWEEN '".$anio."-11-01' and '".$anio."-11-30' and num_paquete = '1' ");
        $datos['noviembre_golds'] = DB::select("SELECT COUNT(id) as noviembre_gold FROM citas WHERE fecha_cita BETWEEN '".$anio."-11-01' and '".$anio."-11-30' and num_paquete = '2' ");
        ////////////// diciembre /////////////////
        $datos['diciembre_basicos'] = DB::select("SELECT COUNT(id) as diciembre_basico FROM citas WHERE fecha_cita BETWEEN '".$anio."-12-01' and '".$anio."-12-31' and num_paquete = '1' ");
        $datos['diciembre_golds'] = DB::select("SELECT COUNT(id) as diciembre_gold FROM citas WHERE fecha_cita BETWEEN '".$anio."-12-01' and '".$anio."-12-31' and num_paquete = '2' ");

       
        return view('home')->with($datos);
    }
}
