<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoSexo extends Model
{
	protected $hidden = ['created_at','updated_at'];
    protected $fillable = ['id','tipo_sexo','activo'];
    protected $table = 'cat_sexo';

   
    
}