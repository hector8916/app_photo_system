<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paquetes extends Model
{
	protected $hidden = ['created_at','updated_at'];
    protected $fillable = ['id','nombre_paquete','precio_paquete','descripcion_paquete','activo'];
    protected $table = 'paquetes';

    
}