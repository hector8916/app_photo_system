<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clientes extends Model
{
	protected $hidden = ['created_at','updated_at'];
    protected $fillable = ['id','nombre_cliente','apellido_paterno','apellido_materno','direccion','cp','fecha_nacimiento','sexo','correo_electronico','telefono','telefono_celular','activo'];
    protected $table = 'clientes';

   public function _Tiposexo(){
    return $this->hasOne('App\TipoSexo', 'id', 'sexo');
  }
    
}